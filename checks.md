# Software Requirements Specification for Book Store Catalogue Page  

## Table of Contents  
[] 2.2 Product Functions  
[] 2.3 User Classes and Characteristics  
[] 2.4 Operating Environment  
[] 2.6 User Documentation  
[] 3. External Interface Requirements  
[] 3.1 User Interfaces  
[] 3.2 Hardware Interfaces  
[] 3.4 Communications Interfaces  
[] 4. System Features  
[] 4.1 Authentication: Login page - **PUBLIC**  
[] 4.2 Authentication: User registration page - **PUBLIC**  
[] 4.3 Catalogue: Book list page - **PROTECTED**  
[] 4.4 Add a new book page - **PROTECTED**  
[] 4.5 Delete a Book - **PROTECTED**  
[] 4.6 About the software page - **PUBLIC**  
[] 4.7 Edit user profile page - **PROTECTED**  
[] 4.8 Logout of system  
[] 5. Other Nonfunctional Requirements  
[] 5.1 Performance Requirements  
[] 5.2 Safety Requirements  
[] 5.3 Security Requirements  
[] 5.4 Business Rules  
[] 7. Appendix B: Data Structures/Models  
[] 7.1 Login Flow  
[] 7.2 Data Structures  

---

### 2.2 Product Functions []
[x] The software will contain a user interface where the store manager* can login into. \
[x] The logged in user should be able to view a list of books available in the store. \
[x] It should also list the books that have been in the store but are not out-of-stock.\
[x] The logged in user must be able to add, edit and delete books from this system. \
[] When adding a book, the user should be able to specify the title, author(s) a short description, an ISBN, \
   and an image for the book. \
[] Displaying the books should list the name, image, author, and the number of copies in stock in a list format \
   that is searchable. \
[x] Clicking on a list item should show the book in a new page and display all the details of the book. \
[x] Users should be able to logout and return to the login screen.\
[x] Once a user is logged out there should be no access to any of the other features of the application.\
[x] The application should also have an About page that must be publicly available. Here should display the\
   details of the developers working on the software, how to contact them and which version of the software is\
   currently running as well as which framework was used to build the project.\

### 2.3 User Classes and Characteristics []  
Users of the application will generally be older staff members with moderate computing experience.\ 
They will receive training on the basics of the system.\
Simplicity should be a key focus of the user interface.\
[] Accessibility would be beneficial but is not required for Phase 1 of the project.\

### 2.4 Operating Environment []  
[] This software will run as a web application and must therefor be able to run in modern web browsers \
[] The application should also be mobile friendly and therefor be able to run on mobile phone web browsers.\

### 2.6 User Documentation []  
A short presentation (15mins) must be prepared briefly discussing the technology used, design choices made \
and demo the basic functionality of the application. This will be hosted on Zoom. \

### 3. External Interface Requirements  []

### 3.1 User Interfaces []
The application will contain six viewable pages.  

[] 1: Login Page  
[] 2: Register Page
[] 3: About Page
[] 4: Catalogue Page (Main Page)
[] 5: Add Book Page
[] 6: Edit User Page  

### 3.2 Hardware Interfaces  []  
[] The program will be run on a web browser, therefor must be compatible with both mouse and touch inputs. \
[] Both physical and software keyboards should be supported. \
[] The output will be rendered on a computer (desktop, laptop etc.) screen as well as mobile devices such as  
phones and tablets. \

### 3.4 Communications Interfaces []  
The application will use HTTP Requests to communicate with the API. All requests will be in JSON format.
Consult the API documentation for Public vs Protected Routes. The API will require an authentication token
to be present in the Authorization Header of the Protected Requests.

### 4. System Features   
This section contains all the features in detail that the system should have available on completion.  

## 4.1 Authentication: Login page - PUBLIC []  
[x] The system must provide a page for the user to login before viewing the available books, adding, editing, or \
deleting books.

### 4.1.1 Description [x]  
The user is expected to navigate to the URL of the application.  
When a user is not logged in, they must be presented with the Login page.  
The user must proceed to fill in their username (email address) along with a matching password.  
The user will then click on the Login button once both the username and password have been filled in.  

### 4.1.2 Functional Requirements []  

[x] REQ-1: Form Validation  
The user is required to fill in both the username, which will be an email address and the \
password. Both fields must be required.  

[x] REQ-2: Access to Register page  
The user must be able to navigate to the register page from the Login page  

[x] REQ-3: Access to About Page  
The user must be able to open the About page from login page.  
[x] It should NOT open in the same browser tab as the login page. It must open in a new tab. \

[x] REQ-4: Login
[x] Entering a valid email and password should allow the user to be successfully logged \
in and be navigated to the Catalogue page. \
[x] On successful login, the application should store the Auth Token in local storage \
received from a successful login HTTP Request for future use and verification of \
logged in status.  

## 4.2 Authentication: User registration page - PUBLIC []  
[x] The system must provide a page for the user to login before viewing the available books, adding, editing, or \
deleting books. \

### 4.2.1 Description [x]  
The user is expected to navigate to the Register page from the Login Page. \
If a user is Registered, they are expected to return to the Login page by clicking on the provided link \
The user must proceed to fill in their Full name, username (email address) along with a matching password. \
There will also be a field to confirm the password to check for any typos in the original password. \
The user will then click on the Register button, once both the username and password have been \
filled in.

### 4.2.2 Functional Requirements []  
 
[x] REQ-1: Form Validation  
[x] The user is required to fill in both the username, which will be an email address and the password. \
[x] Both fields must be required. \

[x] REQ-2: Access to Login page  
   The user must be able to navigate to the Login page from the Register page  

[x] REQ-3: Access to About Page  
   The user must be able to open the About page from Register page.  
[x] It should NOT open in the same browser tab as the Register page. It must open in a new tab.  

[x] REQ-4: Registration  
The user must be able to register for a new account. When the form validation has passed  
successfully the user must be able to click on the Register button. The system to invoke an  
HTTP Request to register the user with the Full name, Email address and password.  
[x] The user must be navigated to the Catalogue Page after completing REQ-5  

[x] REQ-5: Saving the Auth Token  
On Successful Registration, the application should store the Auth Token in local storage  
received from a successful Register Request for future use and verification of logged in  
status  

## 4.3 Catalogue: Book list page - PROTECTED []  
A page to display a list of books.  

### 4.3.1 Description  
[x] The Catalogue Page should contain the books in the store as a list. \
[x] The user should be able to search a book based on a title and author in a search bar.  
[] There should be a checkbox where the user can show only books in stock.  
[] The catalogue page must also show the currently logged in users name, surname, email address  \
and when the last login was. \
[] This should also provide a button to edit the profile a button to logout. \
[x] The Catalogue page should also display a list of books.  
[x] The Add a Book button should navigate the user to the Add new book page.  
[x] It is expected that this page must be displayed after a successful login.  
[] The user will be presented with the list as the focus.  
[] If a user clicks in the search bar, once they start typing it should start filtering the results in the book \
catalogue list.  
[] When a user clicks on the checkbox to show only in-stock books, the list should automatically be \
updated.  
[x] Under the profile section of the page, the user must be able click a button to edit their profile.  
[x] There must also be a button for the user to logout. 
[x] Clicking on the logout buttons should first prompt a confirmation message,  
[x] The user should then choose Confirm or Cancel to either complete the logout or cancel the logout.  
[x] If the user Clicks on the About link, it must open the about page in a new tab. It must NOT replace \
the current page. \

### 4.3.2 Functional Requirements []  

[] REQ-1: Book Catalogue List  
[x] The book catalogue must be a list of books that are in- and out of stock. This page should ONLY be \
accessible if the user has an active Login.  
[x] The Books must be obtained from an HTTP request.  
[x] The list should contain the title, author, in or �out of stock message with the number of items available.  
[x] Each item in the list should have DELETE button.  

[x] REQ-2: Delete Book  
[x] The DELETE button in the Catalogue list should prompt the user to delete a book. 
[x] If the user confirms this should execute an HTTP request to delete the book.  
[x] This book should be removed from the list.  

[] REQ-3: Add Book  
[x] The Add a Book button must take the user to the Add new Book page.  

[] REQ-4: Search Book  
[] Above the list of books there must be a search bar.  
[] The search bar should accept a string that will filter out the list of the books based on the text. \
[] If No matching books are found a message should be shown that there are no books matching that search. \
[] Clearing the Search bar should reset the list of books and display all book items. \
[] Note should be taken of the In Stock checkbox when rerendering the list. \

[] REQ-4: Edit Profile  
[] From the Profile section the user must be able to click on the Edit button and be Navigated to the Edit Profile Page. \

[] REQ-5: Logout  
[] The Profile Section should also give the user the ability to Logout. \
[] This button should remove the Auth Token from the storage and return the user to the Login Page.  
[] They should NOT be able to revisit the Catalogue Page without logging in again.  
[] The logout should also give the user a confirm prompt to Confirm that they do want to logout.  
[] Only if the user clicks Confirm should the logout continue.  

[] REQ-6: Filter In Stock Books  
[] The Filter section of the Catalogue Books page should have a checkbox to only show books currently in stock.  
[] When checked it should automatically filter the list and hide any books that have 0 stock items available.  
[] Unchecking should then redisplay all books.  

[] REQ-7: Access to About Page  
[] The user must be able to open the About page from Catalogue page.  
[] It should NOT open in the same browser tab as the Catalogue page. It must open in a new tab.  

## 4.4 Add a new book page - PROTECTED []  

### 4.4.1 Description  
[x] The add new book page must be a simple form. This page should ONLY Be visible if the user has  
an active login session.  
[x] It should contain the following fields in a form:  
Title, Description, Author(s), ISBN, Image URL, Stock Items and a button to cancel or a button to  
add the book.  
[] The Add new book should also contain a �breadcrumb� at the top of the page.  
Please see the Wireframe for a visual example.  

### 4.4.2 Functional requirements []  

[] REQ-1: Form Validation  
[] All Fields must be required. The user should not be able to add a book without all fields entered.  

[] REQ-2: Adding Book  
[] When adding the Book an HTTP POST Request should be made to the API to add a new book.  
[] Once the book has been added an Alert message should show stating it was successful.  
[] When the user clicks on �OK� they must be navigated back to the Catalogue Book List with the newly added  
   book being visible.  

[] REQ-3: Cancelling  
[] The user must be able to cancel the process by click on the Cancel button.  
[] When a user clicks on Cancel, a confirmation message should appear allowing the user to Confirm that they do want to
   cancel the process.
[] If Confirm is clicked, the user must be navigated back to the Catalogue page.  
[] Clicking Cancel on the confirm message should do nothing.  

## 4.5 Delete a Book - PROTECTED []  

### 4.5.1 Description  
[x] On the Catalogue page the user must be able to delete a book from the list. 
[x] Each Book item in the list must have its own Delete button.  
[x] Clicking Delete will prompt the user to confirm their choice.  

### 4.5.2 Functional Requirements []  

x[] REQ-1: Confirmation  
[x] Clicking on the delete button should prompt the user before actioning the delete. ONLY if the user  
   clicks on Confirm, should the HTTP request to delete the book be executed.  

[x] REQ-2: Deletion  
[x] When the user has confirmed the delete an HTTP Delete Request should be performed to remove the book.  
[x] The list should be updated to reflect that it has been removed  

## 4.6 About the software page - PUBLIC []
An information page and no active login is required to view this page.  

### 4.6.1 Description  
This page will display information about the developers and software.  
[x] It should have the name and emails of any developer that worked on the project.  
[x] There should also be a section displaying the version, what technology was used (React or Angular)  
   and the date the software was completed.  

### 4.6.2 Functional Requirements []  

[x] REQ-1: Developer information  
All the developers that work on the project should be displayed on this page with their name and  
contact email.  

[x] REQ-2: Software information  
There should also be a section displaying the version, what technology was used (React or Angular)  
and the date the software was completed.  

## 4.7 Edit user profile page - PROTECTED []  
A page to change the logged in users� name.  

### 4.7.1 Description []  
The user can access this page from the Catalogue page.  
They may ONLY change their name not their email address.  
 
### 4.7.2 Functional Requirements []  

[] REQ-1: Change Name  
[] The logged in user must be able to change their full name from this page.  
[x] The full name may NOT be empty. 
[x] There can be no numbers in the name.  

[] REQ-2: Saving profile  
[] The user must be able to click an update button that should trigger an HTTP Request that updates their name.  
[] If the name is invalid, they should not be able to save the name.  
  
### 4.8 Logout of system [x]  
Logging out of system is required to remove the auth token and return to the login page.  

### 4.8.1 Description  
When the user clicks on the Logout button, the auth token must be cleared from the local storage.  
The user must then be redirected to the Login page of the system and no longer have access to any  
pages that are protected.  

### 4.8.2 Functional Requirements []  
[x] REQ-1: Logout Button  
Should first prompt the user to confirm if they would like to continue the logout.  

[x] REQ-2: Clearing the session (Auth token)  
When a user has decided to logout, the auth token must be removed from the local storage.  

[x] REQ-3: No Access  
After logging out, no access to any protected pages are allowed.  

### 5. Other Nonfunctional Requirements []  

### 5.1 Performance Requirements []  
The application should load in a reasonable amount of time.  

### 5.2 Safety Requirements []  
Your application should conform to the GDPR rules regarding storing of information. Not Applicable in this  
application.  

### 5.3 Security Requirements []  
The system makes use of JSON Web Tokens and therefor must be submitted with any protected API Request.  
More information on this will be available in the API Documentation.  
The developer is expected to store a valid Auth token received from the API during Login/Register  
that must be submitted as a Bearer Token pattern in the Authorization Header of HTTP Requests.  

### 5.4 Business Rules []  
Only users that are logged in can view, create, and delete books.  
A user may not alter another user�s information.  

### 7. Appendix B: Data Structures/Models []  
### 7.1 Login Flow []  
### 7.2 Data Structures (User, Book) [x]  



