import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  /**
   * Save Session
   * @param session 
   */
  save(session: any) { localStorage.setItem('sb_session', JSON.stringify(session)); }

  /**
   * Get Session
   */
  get(): any {

    const savedSession = localStorage.getItem('sb_session');

    return savedSession ? JSON.parse(savedSession) : false; }

  /**
   * Remove session
   */
  remove(): any { localStorage.removeItem('sb_session'); }

}