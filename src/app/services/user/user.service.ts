import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  updateFullName: any;

  constructor(private http: HttpClient) { }

updateProfileName(user): Promise<any> {
  return this.http.patch( `${environment.apiUrl}/v1/api/users`, {
    user: { 
      ...user }
  }).toPromise()
}


  async logoutUser(): Promise<any> {
  await this.http.patch( `${environment.apiUrl}/v1/api/users/logout`, {
  } 
  ).toPromise()
}

}