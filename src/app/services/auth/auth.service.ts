import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  /**
   * .POST Register
   * @param user 
   */
  register(user): Promise <any> {
  return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
    user:{ ...user }
  }).toPromise();
  }

/**
 * .POST Login
 * @param user 
 */
  login(user): Promise <any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, {
      user:{ ...user }
    }).toPromise();
    }
}
