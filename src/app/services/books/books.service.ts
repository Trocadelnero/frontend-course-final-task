import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap, map, catchError } from "rxjs/operators";
import { throwError, Observable } from 'rxjs';
import { IBooks } from 'src/app/interfaces/books';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient) { }
  /**
   * HTTP.GET list of Books
   */
  getBooks(): Observable<IBooks[]> {
    return this.http.get<IBooks[]>(`${environment.apiUrl}/v1/api/books`)
      .pipe(
        tap(response => {
          console.log(response);
        }),
        map((response: any) => response.data || []),
        catchError(this.errorHandler)
      );
   }
  /**
   * errorHandler
   * @param e 
   */
  private errorHandler(e) {
    let errorMessage: string;
    if (e.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${e.error.message}`;
    } else {
      errorMessage = `Backend returned code ${e.status}: ${e.body.error}`
    }
    console.log(e);
    return throwError(errorMessage);
  }





  

  /** 
   * HTTP.GET a book by id
   * @param id
   */
  getBookById(id): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/books/${id}`)
      .pipe(
        tap(response => {
          console.log(response);
        }),
        map((response: any) => response.data || []),
        catchError(error => {
          if (error.status === 404) {
            return throwError('404. Please try again later.');
          }
          if (error.status >= 500) {
            return throwError('Something is not right. Please try again later.');
          }
          return throwError(error);
        })
      )
      .toPromise()
  }
  /**
   * HTTP.POST , Add book to list
   * @param book 
   */
  addBook(book): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/books`, {
      book: { ...book }
    }
    )
      .toPromise()
  }

  /** 
   * HTTP.DELETE  a book from the list
   * @param book_id
   */
  deleteBookById(book_id): Promise<any> {
    return this.http.delete(`${environment.apiUrl}/v1/api/books/${book_id}`)
      .pipe(
        tap(response => {
          console.log(response);
        }),
        map((response: any) => response.data || []),
        catchError(error => {
          if (error.status === 404) {
            return throwError('404. Please try again later.');
          }
          if (error.status >= 500) {
            return throwError('Something is not right. Please try again later.');
          }

          return throwError(error);

        })
      )
      .toPromise()
  }
}

