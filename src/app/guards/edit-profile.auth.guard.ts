import { EditProfileComponent } from './../components/edit-profile/edit-profile.component';
import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditProfileGuard implements CanDeactivate<EditProfileComponent> {

  canDeactivate(component: EditProfileComponent): Observable<boolean> | Promise<boolean> | boolean {
    
    if (component.editProfileForm.dirty) {
      const full_name = component.editProfileForm.get('full_name').value || 'New Full_name';
      return confirm(`Navigate away and lose all changes to ${full_name}?`);
    }
    return true;
  }
}
