import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBooks } from 'src/app/interfaces/books';
import { BooksService } from 'src/app/services/books/books.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {


  book$: Observable<IBooks>;
  bookDetailError: string;

  constructor(private route: ActivatedRoute, private bookService: BooksService) { }
/**
 * Gets Book Details.
 */
  ngOnInit() {

    this.book$ = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id');
        return this.bookService.getBookById(id);

      })
    )

  }
}







