import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent } from './book-list.component';
import { BookDetailComponent } from '../book-detail/book-detail.component';

const routes: Routes = [
    {
        path: '',
        component: BookListComponent

    }
];

@NgModule({
    declarations: [
       
        BookDetailComponent,
   
    ],

    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        

    ],
    exports: [RouterModule, BookDetailComponent]
})

export class BookListModule { }