import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books/books.service';
import { IBooks } from 'src/app/interfaces/books';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  constructor(private bookService: BooksService, private router: Router) { }



  filteredBooks: IBooks[] = [];
  books$: IBooks[] = [];

  errorMessage = '';
  inStockBooks: boolean = false;

  _searchfilter = '';
  get searchFilter(): string {
    return this._searchfilter
  }
  set searchFilter(value: string) {
    this._searchfilter = value;
    this.filteredBooks = this.searchFilter ? this.getFilteredBooks() : this.books$;
  }





  getFilteredBooks() {
    return this.books$
      .filter(book => {
        return book.title.toLowerCase().includes(this.searchFilter.toLowerCase()) || book.authors.toLowerCase().includes(this.searchFilter.toLowerCase());
      })
      .filter(book => {
        if (this.inStockBooks == false) {
          return book;
        }
        if (this.inStockBooks && book.stock > 0) {
          return book;
        }
      })
  }



  showCover = true;
  toggleCovers() {
    this.showCover = !this.showCover;
  }


  ngOnInit() {
    this.bookService.getBooks().subscribe({
      next: books$ => {
        this.books$ = books$;
        this.filteredBooks = this.books$;

      },
      error: e => this.errorMessage = e
    });
  }



  /**
   * DELETE a specific book by confirmation.
   * @param book_id 
   */
  onDeleteClick(book_id) {
    let confirm = window.confirm("Delete this book from the list? 😢")
    if (confirm) {

      this.bookService.deleteBookById(book_id);
      location.reload();
    } else {
      return confirm = false;
    }
  }
}