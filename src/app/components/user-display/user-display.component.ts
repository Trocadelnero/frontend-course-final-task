import { Component} from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-display',
  templateUrl: './user-display.component.html',
  styleUrls: ['./user-display.component.css']
})
export class UserDisplayComponent {

  constructor(private session: SessionService, private user: UserService, private router: Router) { }


  /**
   * Get User info for display.
   */
  get full_name() {
    return this.session.get().full_name;
  }
  get email() {
    return this.session.get().email;
  }
  get last_login() {
    return this.session.get().last_login;
  }




  onGoAddBookClicked() {
    this.router.navigateByUrl('/add-book');
  }

  onEditProfileClick() {
    this.router.navigateByUrl('/edit-profile');
  }


  onLogoutClick() {

    if (confirm("Logout from our Book Store? 😢")) {
      try {
        this.user.logoutUser();
      } finally {
        this.session.remove()
        this.router.navigateByUrl('/login');
      }

    }

  }
}
