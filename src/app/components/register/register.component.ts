import { SessionService } from './../../services/session/session.service';
import { async } from '@angular/core/testing';
import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerForm: FormGroup = new FormGroup({
    full_name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4)]),
    email: new FormControl('', [Validators.required, Validators.minLength(5)]),
    password: new FormControl('', [Validators.required, Validators.minLength(5)]),
    confirm: new FormControl('')
  })

  isLoading: boolean = false;
  registerError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) {
    //If Session exist redirect to /Catalogue
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  get full_name() {
    return this.registerForm.get('full_name');
  }
  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get confirm() {
    return this.registerForm.get('confirm')
  }



  async onRegisterClicked() {
    this.registerError = '';
    try {
      this.isLoading = true;
      const result: any = await this.auth.register(this.registerForm.value);
      if (result.status < 400) {
        this.session.save({
          token: result.data.token,
          email: result.data.user.email
        });
        this.router.navigateByUrl('/catalogue');
      }
    } catch (e) {
      console.error(e.error);
      this.registerError = e.error.error;

    } finally {
      this.isLoading = false;
    }
  }
}
