import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {


  editProfileForm: FormGroup = new FormGroup({
    full_name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4)])
  });

  isUpdating: boolean = false;
  EditProfileError: string;



  constructor(private router: Router, private session: SessionService, private user: UserService) { }

  get full_name() {
    return this.editProfileForm.get('full_name');
  }
  get current_name() {
    return this.session.get().full_name;
  }
  get email() {
    return this.session.get().email;
  }
  get last_login() {
    return this.session.get().last_login;
  }
  ngOnInit(): void {

  }
  async onUpdateClicked() {
    try {
      this.isUpdating = true;
      if (this.editProfileForm.valid === true) {
        alert('Name will be updated on your next re-visit to our store.')
        const result: any = await this.user.updateProfileName(this.editProfileForm.value)
        console.log(result);
      } else if (this.full_name.valid === false) {
        alert('Enter valid name')
      }
    } catch (e) {
    } finally {
    }
  }







  onCancelClicked() {
    let answer = window.confirm('Pressing OK will abort the process of Editing profile')
    if (answer) {
      this.editProfileForm.reset()
      this.router.navigateByUrl('/catalogue');
    }
    else {
      return answer = false;
    }
  }
}