import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent {

  constructor( private router: Router) { }

  onGoAddBookClicked() {
    this.router.navigateByUrl('/add-book');
  }
  onAboutClicked() {
    this.router.navigateByUrl('/about')

  }


}
