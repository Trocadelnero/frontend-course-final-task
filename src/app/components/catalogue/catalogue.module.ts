import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CatalogueComponent } from './catalogue.component';
import { AddBookComponent } from './../add-book/add-book.component';
import { EditProfileComponent } from './../edit-profile/edit-profile.component';
import { BookListComponent } from '../book-list/book-list.component';
import { UserDisplayComponent } from '../user-display/user-display.component';


const routes: Routes = [
    {
        path: '',
        component: CatalogueComponent
    }
]; 

@NgModule({
    declarations: [CatalogueComponent,
        AddBookComponent,
        BookListComponent,
        EditProfileComponent,
        UserDisplayComponent],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,      
        ReactiveFormsModule,
    ],
    exports: [RouterModule]
})

export class CatalogueModule { }