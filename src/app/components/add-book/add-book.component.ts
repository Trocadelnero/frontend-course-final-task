import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BooksService } from 'src/app/services/books/books.service';
import { IBooks } from 'src/app/interfaces/books';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {


  addBookForm: FormGroup = new FormGroup({
    title: new FormControl(''),
    authors: new FormControl(''),
    cover: new FormControl(''),
    description: new FormControl(''),
    stock: new FormControl(''),
    isbn: new FormControl('')
  });


  book$: IBooks;
  addBookError: string;
  FormGroup: any;

  constructor(private router: Router, private bookService: BooksService) { }

  ngOnInit(): void { }
  //FormGroup Getters
  get title() {
    return this.addBookForm.get('title')
  }
  get authors() {
    return this.addBookForm.get('authors')
  }
  get cover() {
    return this.addBookForm.get('cover')
  }
  get description() {
    return this.addBookForm.get('description')
  }
  get stock() {
    return this.addBookForm.get('stock')
  }
  get isbn() {
    return this.addBookForm.get('isbn')
  }


  async onAddBookClicked() {

    try {
      const result: any = await this.bookService.addBook(this.addBookForm.value);
      console.log(result);
    } catch (e) {
      console.error(e.error);
      this.addBookError = e.error.error
    } finally {
      this.onAddComplete();
    }
  }


  onAddComplete(): void {
    alert('Succes! Book added')
    this.router.navigateByUrl('/catalogue');
  }

  onCancelBookClicked() {
    let answer = window.confirm('Pressing OK will abort the process of adding a book')
    if (answer) {
      this.addBookForm.reset()
      this.router.navigateByUrl('/catalogue');
    }
    else {
      return answer = false;
    }
  }
}

