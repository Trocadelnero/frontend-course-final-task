import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBookComponent } from './add-book.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: '',
        component: AddBookComponent
    }
];

@NgModule({
    
    imports: [ 
    RouterModule.forChild( routes ),
    CommonModule,
    FormsModule,
    ReactiveFormsModule],
    exports: [ RouterModule ]
})

export class AddBookModule {}
