export interface IUser {
    id: number;
    full_name: string;
    email: string;
    last_login: string;
    active: number;
    created_at: string;
    updated_at: string;
}
