export interface IBooks {
    id: number;
    title: string;
    authors?: string;
    cover: string;
    created_at: string;
    description: string;
    favourite: boolean;
    genres: string;
    isbn: string;
    stock: number;
    active: number;
    updated_at: string;

}
