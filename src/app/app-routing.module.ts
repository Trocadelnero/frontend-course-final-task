import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';

const routes: Routes = [
  {
    path: 'about',
    loadChildren: () => import('./components/about/about.module').then(m => m.AboutModule),   
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./components/catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'add-book',
    loadChildren: () => import('./components/add-book/add-book.module').then(m => m.AddBookModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'book-list',
    loadChildren: () => import('./components/book-list/book-list.module').then(m => m.BookListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'book-detail/:id',
    component: BookDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit-profile',
    component: EditProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
